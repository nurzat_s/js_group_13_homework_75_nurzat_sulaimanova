const express = require('express');
const app = express();
const messages = require('./app/messages');
const cors = require('cors');

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/messages', messages);

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
})