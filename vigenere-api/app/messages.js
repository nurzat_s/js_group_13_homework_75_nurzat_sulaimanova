const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const router = express.Router();

router.post('/encode', (req, res) => {
  console.log(req.body)
  return res.send({encode: Vigenere.Cipher(req.body.password).crypt(req.body.message)});
});

router.post('/decode', (req, res) => {
  console.log(req.body)
  return res.send({decode:Vigenere.Decipher(req.body.password).crypt(req.body.message)});

});

module.exports = router;