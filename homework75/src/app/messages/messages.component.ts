import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessagesService } from '../services/messages.service';
import { Encode, Message } from '../models/message.model';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit {
  @ViewChild('f') form! : NgForm;


  constructor(private messageService: MessagesService) { }

  ngOnInit(): void {
  }


  encodeMessage() {
    const messageData = new Message(
      this.form.value.password,
      this.form.value.message
    )
    this.messageService.encode(messageData).subscribe((response) => {
        this.form.value.message = response.encode;
      console.log(response)
    })

  }

  decodeMessage() {
    const messageData = new Message(
      this.form.value.password,
      this.form.value.message
    )
    this.messageService.decode(messageData).subscribe((response) => {
      this.form.value.message = response.decode;
      console.log(response)
    })
  }
}
