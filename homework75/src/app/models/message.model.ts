export class Message {
  constructor(
    public password: string,
    public message: string,
  ) {
  }
}

export interface MessageData {
  password: string,
  message: string,
}

export class Encode{
  constructor(
    public encode: string
  ) {}
}

export class Decode{
  constructor(
    public decode: string
  ) {}
}
