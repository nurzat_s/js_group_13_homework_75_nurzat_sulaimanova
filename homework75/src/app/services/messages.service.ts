import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Decode, Encode, Message, MessageData } from '../models/message.model';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private http: HttpClient) { }

  decode(messageData: MessageData) {
    return this.http.post<Decode>(environment.apiUrl + '/messages/decode', messageData);
  }

  encode(messageData: MessageData) {
    console.log(messageData)
    return this.http.post<Encode>(environment.apiUrl + '/messages/encode', messageData);
  }
}
